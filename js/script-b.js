$( document ).ready(function() {
  // Parse the URL parameters into urlParamsArr
  var urlParamsArr = {};
  var urlParams = document.location.search.substr(1).split("&");
  for (var i = 0; i < urlParams.length; i++) {
    var ret = urlParams[i].toString().split("=");
    urlParamsArr[ret[0]] = decodeURIComponent(ret[1]);
  }
  // Inject the user's nickname
  $('#nickname').html(urlParamsArr["nickname"]);
  // Inject the login provider
  $('#loginProvider').html(urlParamsArr["loginProvider"]);
  // Inject the login provider
  $('#email').html(urlParamsArr["email"]);

  reloadConnectedList();
});

function logOut() {
  gigya.socialize.logout({callback: onLogOut});
}

function onLogOut(response) {
  if ( response.errorCode == 0 ) {
    window.location='a.html';
  }
  else {
    alert('Error :' + response.errorMessage);
  }
}

function reloadConnectedList() {
  gigya.socialize.getUserInfo({callback:printConnectedList});
}
function printConnectedList(response) {
  if ( response.errorCode == 0 ) {
      var user = response['user'];
      var provider = '';
      $("#connected-list").empty();
      if(user.identities) {
        for(i in user.identities) {
          provider = user.identities[i].provider.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
          });
          $('#connected-list').append('<li>'+provider+'</li>');
        }
      }
      var msg = 'User '+user['nickname'] + ' is ' +user['age'] + ' years old';
  } else {
      alert('Error :' + response.errorMessage);
  }
}
function shareThisPage() {
  var act = new gigya.socialize.UserAction();
  act.setTitle("Gigya demo test portal");
  act.setLinkBack("https://demo.gigya.com/about.php");
  act.setDescription("This is my Description");
  act.addActionLink("Read More", "https://demo.gigya.com/about.php");

  act.addMediaItem( { type: 'image', src: 'https://demo.gigya.com/images/300x250_myoss_3frames-lg.gif', href: 'https://demo.gigya.com/about.php' });

  var params =
  {
      userAction:act
      ,showMoreButton: true
      ,showEmailButton: true
  };

  gigya.socialize.showShareUI(params);
}
