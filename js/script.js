
var globalName = '';
var globalEmail = '';
var globalProvider = '';

function onLoginHandlerSocial(eventObj) {
  globalName = eventObj.user.nickname;
  globalEmail = eventObj.user.email;
  globalProvider = eventObj.provider;
  if(!globalEmail || globalEmail=='') {
    $('#form-register-div').hide();
    $('#form-login-div').hide();
    $('#social-div').hide();
    $('#form-email-div').fadeIn();
  } else {
    redirectToB();
  }
}
function onLoginHandlerLocal(obj) {

  // 1. Save session details
  // this step is skipped for this demo

  // 2. Redirect to B
  globalName = $('#reg_name').val();
  globalEmail = $('#reg_email').val();
  globalProvider = 'login';
  redirectToB();
}
function redirectToB() {

  // Update counter
  var count = Cookies.get("site-login-count");
  count = (count) ? (parseInt(count)+1) : 1;
  Cookies.set('site-login-count', count);

  // Redirect to B
  window.location='b.html?nickname='+globalName+'&email='+globalEmail+'&loginProvider='+globalProvider;
  return false;
}

$(function() {
  $("form[name='form-register']").validate({
    rules: {
      name: "required",
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 6
      },
      password_again: {
        equalTo: "#password",
        required: true,
        minlength: 6
      }
    },
    messages: {
      name: "Please enter your full name",
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long"
      },
      email: "Please enter a valid email address"
    },
    submitHandler: function(form) {

      // 1. Connect to back-end API via Ajax to complete the registration and receive UID
      // this step is skipped for this demo

      // 2. Prepare data
      var dateStr = Math.round(new Date().getTime()/1000.0);  // Current time in Unix format
      var siteUID= 'uTtCGqDTEtcZMGL08w';  // siteUID should be taken from the new user record

      // 3. Update Gigya SDK to notify about new local registration
      var params = {
          siteUID: siteUID,
          UIDTimestamp:dateStr,
          callback: onLoginHandlerLocal
      };
      gigya.socialize.notifyRegistration(params);
      return false;
    }
  });
  $("form[name='form-login']").validate({
    rules: {
      email: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 6
      }
    },
    messages: {
      password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long"
      },
      email: "Please enter a valid email address"
    },
    submitHandler: function(form) {

      // this step is skipped for this demo

      $("#login-registered-error").fadeIn();
      return false;
    }
  });
  $("form[name='form-email']").validate({
    rules: {
      email: {
        required: true,
        email: true
      }
    },
    messages: {
      email: "Please enter a valid email address"
    },
    submitHandler: function(form) {
      globalEmail = $('#social_email').val();
      redirectToB();
      return false;
    }
  });

  $( "#link-register" ).click(function(event) {
    event.preventDefault();
    $("#form-login-div").hide();
    $("#form-register-div").fadeIn();
  });
  $( "#link-login" ).click(function(event) {
    event.preventDefault();
    $("#form-register-div").hide();
    $("#form-login-div").fadeIn();
  });

});
